const formWrapper = document.querySelector('.reg-form-wrapper');
const errBox = document.querySelector('.reg-form-error-box');
const passInput = document.getElementById('reg-form-input');
const regBtn = document.getElementById('reg-form-btn');

const showCards = function () {
    formWrapper.insertAdjacentHTML('afterend', `
        <div class="items-wrapper">
            <div class="items">
                <a href="https://aymob.com/" class="card unclickable">Business</a>
                <a href="https://aymob.com/" class="card unclickable">Staging environment</a>
                <a href="https://aymob.com/" class="card clickable">Main site</a>
                <a href="https://docs.google.com/spreadsheets/d/1ynLfJ6OofpdXG2byuplA2M4v-tJKu5_8wZzaDLQ-MTo/edit?usp=sharing"
                    class="card clickable">Thank You Page Manager</a>
                <a href="https://docs.google.com/spreadsheets/d/1qwTk9tlzbhtxxYkqVuHRk1Jjm07Nn4mm0KDGjRmxevg/edit?usp=sharing"
                    class="card clickable">GTM Manager</a>
                <a href="https://docs.google.com/spreadsheets/d/1qWDe2LrDhk65XNOJlP5azOFcjXGmDKGhQCRYL1TeuZw/edit?usp=sharing"
                    class="card clickable">Trackbox Manager</a>
                <a href="https://drive.google.com/drive/folders/1ZUAliS0HRYOXSsEb4sCOWRYNZP61djvQ"
                    class="card clickable">Creatives and Designs</a>
                <a href="https://bitbucket.org/dashboard/projects" class="card clickable">BitBucket</a>
            </div>
        </div>
    `);
    formWrapper.style.display = 'none';
}

const init = function (dataObject) {
    let parsedObject = '';
    hideErr();
    if(localStorage.getItem('dataObject')) {
        parsedObject = JSON.parse(localStorage.getItem('dataObject'));
        if (parsedObject.auth === 'true') {
            showCards();
        }
        if (parsedObject.auth === 'false') {
            regBtn.addEventListener('click', function (e) {
                e.preventDefault();
                checkPass(parsedObject, dataObject);
            });
        }
    } else {
        localStorage.setItem('dataObject', JSON.stringify(dataObject));
        parsedObject = JSON.parse(localStorage.getItem('dataObject'));
        regBtn.addEventListener('click', function (e) {
            e.preventDefault();
            checkPass(parsedObject, dataObject);
        });
    }
}

const checkPass = function (parsedObject, dataObject) {
    if (passInput.value && passInput.value !== parsedObject.pass ) {
        showErr("You're not supposed to be here.<br/> If you are, you should know the password.");
    } else if (!passInput.value) {
        showErr(" The password is empty!");
    } else {
        hideErr();
        dataObject = { 'pass': 'aymob', 'auth': 'true' };
        localStorage.setItem('dataObject', JSON.stringify(dataObject));
        errBox.innerHTML = '';
        passInput.value = '';
        showCards();
    }
}

const showErr = function (errText) {
    errBox.style.display = 'flex';
    errBox.innerHTML = `<svg viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle cx="17" cy="17" r="17" fill="white"/>
    <g clip-path="url(#clip0_48:60)">
    <path d="M25.6402 20.115L19.334 9.19223C18.304 7.38783 15.6993 7.38783 14.6729 9.19223L8.36308 20.115C7.33305 21.9194 8.61503 24.161 10.6936 24.161H23.2874C25.366 24.161 26.6702 21.8972 25.6402 20.115ZM16.9998 21.7379C16.4514 21.7379 15.9957 21.2821 15.9957 20.7338C15.9957 20.1854 16.4514 19.7297 16.9998 19.7297C17.5481 19.7297 18.0039 20.1854 17.9816 20.7597C18.0076 21.2821 17.5259 21.7379 16.9998 21.7379ZM17.9149 15.2464C17.8705 16.0245 17.8223 16.7989 17.7778 17.577C17.7556 17.8289 17.7556 18.0586 17.7556 18.3069C17.7334 18.7182 17.411 19.0368 16.9998 19.0368C16.5885 19.0368 16.2699 18.7404 16.2439 18.3291C16.1772 17.1175 16.1068 15.9282 16.0401 14.7166C16.0179 14.398 15.9957 14.0756 15.9697 13.757C15.9697 13.2308 16.2662 12.7973 16.7478 12.6602C17.2295 12.5454 17.7075 12.7751 17.9149 13.2308C17.9853 13.3902 18.0076 13.5495 18.0076 13.7347C17.9853 14.2423 17.9372 14.7462 17.9149 15.2464Z" fill="#fb0046"/>
    </g>
    <defs>
    <clipPath id="clip0_48:60">
    <rect width="18" height="18" fill="white" transform="translate(8 7)"/>
    </clipPath>
    </defs>
    </svg> ${errText}`;
}
const hideErr = function () {
    errBox.style.display = 'none';
    errBox.innerHTML = '';
}

let creds = { 'pass': 'aymob', 'auth': 'false' };
init(creds);